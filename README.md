# ECL Pipe Library

A small library which implements a wrapper around system calls to deal with inter process communication.
The aim of this project is to make easy to communicate between processes in a hardware testbench.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for further use in a testbench.

### Cloning/copying the project source files:

If you have a configured ssh acount for git, you can clone our repository as follows:
```console
~$ git clone git@gitlab.com:ecl-video-coding/testing-pipe-repo.git
```

Otherwise, you may clone as:
```console
~$ git clone https://gitlab.com/ecl-video-coding/testing-pipe-repo.git
```

You may as well download the project as a \*.zip file. 

### Compilation:

After cloning our repository:
```console
~$ cd testing-pipe-repo
~/testing-pipe-repo$ make
```

This will compile the source into a simple test case (producer-consumer) and the library.


### Installing the library:

This step is not mandatory, but may help to use this library.
Thus, if you want, you may install the library in your system using our makefile. This may required privileged access (sudo):

```console
~/testing-pipe-repo$ make install-lib
```

The above make command will copy the header file (`eclpipes.h`) and the library file (`liblibeclpipes.a`) into the system folder (the default configured in the makefile is `/usr/local/include` and `/user/local/lib` ) and user folder (defaults: `/usr/include` and `/user/lib`).

## Using the Library in your code and compiling your testbench

You just need to include our header `eclpipes.h` in both communication ends (reference software and software testbench) and compile both using the flag `-leclpipes`. If you do not installed the library, you must tell the linker where the library is using the flag `-L`. Similarly, you can tell gcc where the library header is using the flag `-I`.

If you are not being successful compiling, this may help you: http://www.network-theory.co.uk/docs/gccintro/gccintro_21.html

For further assistance, please contact us.

Now for how to use the library. In the producer side (reference software) you nedd to call the `write_to` function. Let us analyze an simple example:

```c
#include <eclpipes.h>
...
int value=42;
write_to("filename", &value, sizeof(int));
```

This writes the number 42 in the pipe called `"filename"`. Notice that the `write_to` function is a blocking function. I.e., it will block until someone is able to read the value written into the pipe.

In the consumer side (testbench software), to read one integer you will need only to call the `read_from` function, as follows:

```c
#include <eclpipes.h>
...
int value;
read_from("filename", &value, sizeof(int));
...
unlink_pipes();
```

Notice that the `unlink_pipes` should only be called after reading all the required data. You may as well not call this function. It serves only to remove the created pipe files. In a further execution, if both writer and reader (producer and consumer) were previously killed (terminated), the pipe will start clean even without calling `unlink_pipes`.

## Authors

* **Bruno Bonotto** - *Initial work and research on IPC Mechanisms*
* **Ismael Seidel** - *Library code*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

If you used our library in an academic work, please cite us as:

> B. Bonotto, L. H. de Lorenzi Cancellier, M. Monteiro, I. Seidel, J. L. Güntzel, “A named-pipe library for hardware simulation”, in Proceedings of the 33th South Symposium on Microelectronics (SIM), Curitiba, Brazil: SBC, 2018

Or, better yet, use this [bib file](bib.bib).


## Acknowlegements

To the co-authors of this work:

* **Marcio Monteiro**
* **Luiz Henrique de Lorenzi Cancellier**
* **José Luís A. Güntzel** - Advisor

## TODO

* Provide a more realist use example;
* Describe how to execute the `main` to see if everything is working properly;
* Add prerequisites;
* Better document the library;

