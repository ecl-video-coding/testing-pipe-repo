#include "eclpipes.h"

/**@file
 * @brief [brief description]
 * @details [long description]
 * 
 */


//private declarations;
void create_fifo_and_check_for_possible_errors(const char* filename);
int  open_file_descriptor(const char* filename, int flags);
void close_file_descriptor(int fd);
int write_to_pipe_file_descriptor(int fd, unsigned long size_of_data, const void* data_to_write);
int read_from_pipe_file_descriptor(int fd, unsigned long size_of_data, void* read_data);
void unlink_pipe(const char* filename);

char* copy_string(const char* string_to_copy);
void reallocate_lists();
int open_new_fd(const char* filename, int flags);
int get_fd_from_filename(const char* filename);


eclvc_pipe_handler_t eclvc_pipe_handler = {0, 0, 0, 0}; //declared as extern in the header


/**
 * @brief Tries to create the FIFO and check for possible errors
 * @details [long description]
 * 
 * @param filename The complete filename (including path) to the FIFO
 */
void create_fifo_and_check_for_possible_errors(const char* filename)
{
	int mk_fifo_result=mkfifo(filename, FIFO_PERMISSIONS);

	if (mk_fifo_result == -1) //some error happened, perhaps the file already exists
	{
		if(errno == EEXIST)
		{
			struct stat sb;
			if (stat(filename, &sb) == -1) {	//On success, zero is returned. On error, -1 is returned, and errno is set appropriately.
		        fprintf(stderr, "The following error occured when trying to stat the already existing file \"%s\" during write.\n", filename); 
		        exit(EXIT_FAILURE);
		    }
			if((sb.st_mode & S_IFMT) != S_IFIFO) {
				fprintf(stderr, "There was an error during write. The file \"%s\" already exists and it is not a named-pipe!\n", filename);
				exit(EXIT_FAILURE);
			}
		} else {
			fprintf(stderr, "The following error occured when trying to open \"%s\": %s.\n", filename, strerror(errno)); 
			exit(EXIT_FAILURE);
		}
	}

}


int open_file_descriptor(const char* filename, int flags)
{
	int fd = open(filename, flags);
	if (fd == -1)
	{
		fprintf (stderr, "Error opening file descriptor (%s): %s\n", filename, strerror(errno));
		exit(EXIT_FAILURE);
	}
	return fd;
}

void close_file_descriptor(int fd)
{
	close(fd);
}

void unlink_pipe(const char* filename)
{
	// Removes the pipe in filename.
    unlink(filename);
}

int write_to_pipe_file_descriptor(int fd, unsigned long size_of_data, const void* data_to_write)
{
	int control = 0;
	
	control = write(fd, data_to_write, size_of_data);

	if (control == -1)
	{
		//EPIPE: broken pipe
		if (errno == EPIPE)
		{
			return -1; //this means that there is no reader in the other side of the pipe...
		}
		fprintf (stderr, "Error writing to file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return control;
}



int read_from_pipe_file_descriptor(int fd, unsigned long size_of_data, void* read_data)
{
	int control = 0;
	
	control = read(fd, read_data, size_of_data);

	if (control == -1)
	{
		fprintf (stderr, "Error writing to file: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	return control;
}


/**
 * @brief Writes the data pointed by data_to_write into the FIFO with name 'filename'
 * @details [long description]
 * 
 * @param filename The complete filename (including path) to the FIFO
 * @param data_to_write A const pointer to the data which will be written into the FIFO
 * @param size_of_data Number of bytes to be written
 * @return The number of bytes written into the FIFO. If some errror happend, it will return -1
 */
int write_to(const char* filename, const void* data_to_write, unsigned long size_of_data)
{

	int fd = get_fd_from_filename(filename);
	if(fd == -1) //means it is not open yet
	{
		create_fifo_and_check_for_possible_errors(filename);
		fd = open_new_fd(filename, O_WRONLY); //it now adds to the handler!
	}
	return write_to_pipe_file_descriptor(fd, size_of_data, data_to_write);
}


/**
 * @brief [brief description]
 * @details [long description]
 * 
 * @param filename [description]
 * @param read_data [description]
 * @param size_of_data [description]
 * @return [description]
 */
int read_from(const char* filename, void* read_data, unsigned long size_of_data)
{
	int fd = get_fd_from_filename(filename);
	int control = 0;
	if(fd == -1) //means it is not open yet
	{
		create_fifo_and_check_for_possible_errors(filename);
		fd = open_new_fd(filename, O_RDONLY); //it now adds to the handler!
		//first read is a bit different, needs to block until read some byte...
		while((control = read_from_pipe_file_descriptor(fd, size_of_data, read_data)) == 0); //do nothing...
		return control;
	}
	return read_from_pipe_file_descriptor(fd, size_of_data, read_data);
}


//returns a new allocated pointer to the copied string
char* copy_string(const char* string_to_copy)
{
//The C library function size_t strlen(const char *str) computes the length of the string str up to, 
//but not including the terminating null character.
	size_t string_lenght = strlen(string_to_copy);
	char* copied_string = (char*) malloc((string_lenght+1)*sizeof(char)); // need one extra byte for the null char

	strcpy(copied_string, string_to_copy);

	return copied_string;
}


void reallocate_lists()
{
	int current_number_of_itens = eclvc_pipe_handler.mallocated_list_elements;
	int new_number_of_itens = current_number_of_itens << 1; // equivalent to x2
	int* new_list_of_fds = (int*) malloc(new_number_of_itens*sizeof(int));
	char** new_list_of_filenames = (char**) malloc(new_number_of_itens*sizeof(char*));

    int* current_list_of_fd = eclvc_pipe_handler.list_of_file_descriptors;
    char** current_list_of_filenames = eclvc_pipe_handler.list_of_open_filenames;

    int i=0;
    for(;i<current_number_of_itens;++i)
    {
    	new_list_of_fds[i] = current_list_of_fd[i];
		new_list_of_filenames[i] = current_list_of_filenames[i];
    }
    free(current_list_of_fd);
    free(current_list_of_filenames);

    eclvc_pipe_handler.list_of_file_descriptors=new_list_of_fds;
	eclvc_pipe_handler.list_of_open_filenames=new_list_of_filenames;

	eclvc_pipe_handler.mallocated_list_elements=new_number_of_itens;
}


int open_new_fd(const char* filename, int flags)
{
	if (eclvc_pipe_handler.number_of_active_file_descriptors == 0) //this is the first call for this function. Need to initialize many things
	{
		eclvc_pipe_handler.list_of_file_descriptors = (int*) malloc(INITIAL_MALLOC_SLOTS*sizeof(int));
		eclvc_pipe_handler.list_of_open_filenames = (char**) malloc(INITIAL_MALLOC_SLOTS*sizeof(char*));
		eclvc_pipe_handler.mallocated_list_elements = INITIAL_MALLOC_SLOTS;
	}

	int current_fd_index = eclvc_pipe_handler.number_of_active_file_descriptors;
	eclvc_pipe_handler.number_of_active_file_descriptors++;

	if (eclvc_pipe_handler.number_of_active_file_descriptors >= eclvc_pipe_handler.mallocated_list_elements)
	{
		reallocate_lists();
	}

	//open_fd?
	int new_fd = open_file_descriptor(filename, flags);

	eclvc_pipe_handler.list_of_file_descriptors[current_fd_index]=new_fd;
	eclvc_pipe_handler.list_of_open_filenames[current_fd_index]=copy_string(filename);

	return new_fd;
}


//returns file descriptor or -1, if no fd is found open with such filename
int get_fd_from_filename(const char* filename)
{
	int current_number_of_open_fds = eclvc_pipe_handler.number_of_active_file_descriptors;
	int i = 0;
	for(;i<current_number_of_open_fds;++i)
	{
		if(strcmp(filename, eclvc_pipe_handler.list_of_open_filenames[i]) == 0)
			return eclvc_pipe_handler.list_of_file_descriptors[i];
	}
	return -1;
}


void unlink_pipes()
{
	int current_number_of_open_fds = eclvc_pipe_handler.number_of_active_file_descriptors;
	int i=0;
	for(;i<current_number_of_open_fds;++i)
	{
		unlink_pipe(eclvc_pipe_handler.list_of_open_filenames[i]);
	}
}