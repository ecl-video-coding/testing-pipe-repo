#ifndef ECLPIPES_H__
#define ECLPIPES_H__

#include <fcntl.h>  // O_RDONLY - file control options
#include <sys/stat.h>  // mkfifo() - data returned by the stat() function
#include <unistd.h>  // read() - standard symbolic constants and types
#include <stdio.h>  // printf()
#include <errno.h>  // errno - system error number
#include <string.h> // strerror(errno)
#include <stdlib.h> // EXIT_FAILURE




/*
/ 0666 (octal) means the following permissions:
/	S_IRUSR (file reading - 0400) | S_IWUSR (file writing - 0200) |
/	S_IRGRP (file group reading - 0040) | S_IWGRP (file group writing - 0020) |
/	S_IROTH (reading for other users - 0004) | S_IWOTH (writing for other users- 00002)
/ more info in http://www.gnu.org/software/libc/manual/html_node/Permission-Bits.html
*/
#define FIFO_PERMISSIONS 0666
#define INITIAL_MALLOC_SLOTS 4


typedef struct {
	size_t number_of_active_file_descriptors;//=0;
	size_t mallocated_list_elements;//=0;
	int* list_of_file_descriptors;//=0;
	char** list_of_open_filenames;//=0;
} eclvc_pipe_handler_t;

//typedef struct eclvc_pipe_handler eclvc_pipe_handler_t;

extern eclvc_pipe_handler_t eclvc_pipe_handler;

#ifdef __cplusplus
extern "C" {
#endif
	//public interface
	int read_from(const char* filename, void* read_data, unsigned long size_of_data);

	int write_to(const char* filename, const void* data_to_write, unsigned long size_of_data);

	void unlink_pipes();
#ifdef __cplusplus
}
#endif

//void add_new_filename(const char* filename);


#endif /* end of include guard: ECLPIPES_H__ */

// void create_fifo_and_check_for_errors(const char* filename);
// int  open_file_descriptor(const char* filename, int flags);
// void close_file_descriptor(int fd);
// void write_to_pipe_file_descriptor(int fd, size_t size_of_data, void* data_to_write);
// void read_from_pipe_file_descriptor(int fd, size_t size_of_data, void* read_data);
// void unlink_pipe(const char* filename);

// char* copy_string(const char* string_to_copy);
// void reallocate_lists();
// int open_new_fd(const char* filename);
// int get_fd_from_filename(const char* filename);