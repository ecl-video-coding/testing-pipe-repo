/*
* @Author: ismaelseidel
* @Date:   2018-03-13 14:01:05
* @Last Modified by:   ismaelseidel
* @Last Modified time: 2018-03-13 16:07:24
*/

#include "eclpipes.h"
#include <assert.h>
#include <sys/wait.h>

#define TEST_PIPE_NAME "test_pipe"
#define TEST_VALUE 42

#define NUMBER_OF_PIPES_TO_TEST 10

char* filenames[NUMBER_OF_PIPES_TO_TEST] = {
	"test_pipe_1", 
	"test_pipe_2", 
	"test_pipe_3", 
	"test_pipe_4", 
	"test_pipe_5", 
	"test_pipe_6", 
	"test_pipe_7", 
	"test_pipe_8", 
	"test_pipe_9", 
	"test_pipe_10" 
};


int numbers_to_test[NUMBER_OF_PIPES_TO_TEST] = {
	10,
	9,
	8,
	7,
	6,
	5,
	4,
	3,
	2,
	1
};


void test_reader() 
{

	int value_to_read = 0;

	//single test
	read_from(TEST_PIPE_NAME, &value_to_read, sizeof(int));
	assert(value_to_read == TEST_VALUE);

	int i=0;
	for(;i<NUMBER_OF_PIPES_TO_TEST;++i)
	{
		read_from(filenames[i], &value_to_read, sizeof(int));
		sleep(1);
		printf("i = %d\n", i);
		assert(value_to_read == numbers_to_test[i]);
	}

	//
	unlink_pipes(); // if alread readed the value, it can remove the pipe..
}

void test_writer() 
{
	int value_to_write = TEST_VALUE;
	write_to(TEST_PIPE_NAME, &value_to_write, sizeof(int));

	int i=0;
	for(;i<NUMBER_OF_PIPES_TO_TEST;++i)
	{
		value_to_write=numbers_to_test[i];
		write_to(filenames[i], &value_to_write, sizeof(int));
	}
}

int main(int argc, char const *argv[])
{

	pid_t pid;

	if ((pid = fork()) < 0) {
        fprintf(stderr, "There was an error trying to create a child process: %s", strerror(errno));
        exit(EXIT_FAILURE);
    }
    if (pid == 0) {
        // Child process, will create a reader...
        //printf("child's pid: %d\n", getpid());
		test_reader();
    }
    else
    {
        // Father process, will create a writer...
        //printf("father's pid: %d\n", getpid());
		test_writer();
	    // int status;
	    // wait(&status);
    }


	return 0;
}