SRCDIR=./src
OBJDIR=./obj
DISTDIR=./dist
LIBDIR=./lib

LIBS= 
#-lm

COMPILER=gcc
FLAGS=-Wall -g


SRC=$(subst .c,,$(notdir $(wildcard $(SRCDIR)/*.c)))

ALLOBJS=$(addprefix $(OBJDIR)/, $(addsuffix .o,$(SRC)))
READEROBJS=$(filter-out $(OBJDIR)/writer.o,$(ALLOBJS))
WRITEROBJS=$(filter-out $(OBJDIR)/reader.o,$(ALLOBJS))

LIBOBJS=$(filter-out $(OBJDIR)/main.o,$(ALLOBJS))
SYSTEMDIR=/usr/local

all: main lib

clean: clean-objs

clean-objs:
	rm -rf $(OBJDIR)/*

clean-dir:
	rm -rf $(DISTDIR)/*

clean-libs:
	rm -rf $(LIBDIR)/*

veryclean: clean-objs clean-dir clean-libs

lib: $(LIBDIR)/libeclpipes.a

$(LIBDIR)/libeclpipes.a: $(ALLOBJS)
	ar -cvq $(LIBDIR)/libeclpipes.a $(LIBOBJS)	

install-lib: $(LIBDIR)
	cp $(SRCDIR)/eclpipes.h $(SYSTEMDIR)/include/eclpipes.h
	cp $(LIBDIR)/libeclpipes.a $(SYSTEMDIR)/lib/libeclpipes.a
	cp $(SRCDIR)/eclpipes.h /usr/include/eclpipes.h
	cp $(LIBDIR)/libeclpipes.a /usr/lib/libeclpipes.a

main: $(ALLOBJS)
	$(COMPILER) $(FLAGS) -o $(DISTDIR)/main $^ $(LIBS)

$(OBJDIR)/%.o : $(SRCDIR)/%.c 
	$(COMPILER) $(FLAGS) -c -o $@ $<